from setuptools import setup

with open('README.md') as f:
    README = f.read()

with open('requirements.txt') as requirements:
   required = requirements.read().splitlines()

setup(
    name='nick',
    version='0.1.0',
    url='https://gitlab.com/underyx/nick',
    author='Bence Nagy, Simone Esposito',
    author_email='bence@underyx.me, chaufnet@gmail.com',
    description='A CLI tool that suggests you shell aliases based on your shell history',
    long_description=README,
    packages=['nick'],
    install_requires=required,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'License :: OSI Approved :: MIT License',
        'Operating System :: Unix',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ]
)
